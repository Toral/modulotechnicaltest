//
//  ProductType.swift
//  moduloTechnicalTest
//
//  Created by Alban on 03/08/2021.
//

import Foundation

enum ProductType: String, CaseIterable {
    case heater = "Heater"
    case rollerShutter = "RollerShutter"
    case light = "Light"
    
    static func stringToEnum(deviceType: String) -> ProductType? {
        switch deviceType {
        case "Heater":
            return .heater
        case "Light":
            return .light
        case "RollerShutter":
            return .rollerShutter
        default:
            return nil
        }
    }
}
