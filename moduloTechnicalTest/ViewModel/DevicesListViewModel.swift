//
//  DevicesListViewModel.swift
//  moduloTechnicalTest
//
//  Created by Alban on 04/08/2021.
//

import Foundation
import RxSwift

struct DevicesListViewModel {
    private let devices: [Device]

    init(devices: [Device]) {
        self.devices = devices
    }
}
