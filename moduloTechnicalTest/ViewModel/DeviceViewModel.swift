//
//  DeviceViewModel.swift
//  moduloTechnicalTest
//
//  Created by Alban on 04/08/2021.
//

import Foundation
import RxRelay

struct DeviceViewModel {
    var device: Device
    
    var deviceName: String {
        return device.deviceName
    }
    
    var type: ProductType? {
        return ProductType.stringToEnum(deviceType: device.productType)
    }
    
    // Tried to user BehaviorRelay to see an update
    // BehaviorRelay previously Variable
    var temperature: BehaviorRelay<Int> {
        return BehaviorRelay(value: device.temperature ?? 7)
    }
    
    init(device: Device) {
        self.device = device
    }
}
