//
//  UserViewModel.swift
//  moduloTechnicalTest
//
//  Created by Alban on 07/08/2021.
//

import Foundation

struct UserViewModel {
    private var user: User
    
    var firstName: String {
        return user.firstName
    }
    
    var lastName: String {
        return user.lastName
    }
    
    var streetName: String {
        return user.address.street
    }
    
    var streetCode: String {
        return user.address.streetCode
    }
    
    var country: String {
        return user.address.country
    }
    
    var city: String {
        return user.address.city
    }
    
    var birthDate: String {
        let unixTimestamp = Double(String("\(user.birthDate)".reversed())) ?? 0
        let date = Date(timeIntervalSince1970: unixTimestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        return dateFormatter.string(from: date)
    }
    
    init(user: User) {
        self.user = user
    }
}
