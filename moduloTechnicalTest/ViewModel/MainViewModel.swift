//
//  MainViewModel.swift
//  moduloTechnicalTest
//
//  Created by Alban on 03/08/2021.
//

import Foundation
import RxSwift

final class MainViewModel {
    
    // Instanciate in the init, can be force unwrapped
    private let mainService: MainService!
    private let modelObservable: Observable<Model>
    
    init(mainService: MainService = MainService()) {
        self.mainService = mainService
        modelObservable = mainService.request()
    }
    
    func fetchDevicesViewModel() -> Observable<[DeviceViewModel]> {
        modelObservable.compactMap { $0.devices.map { DeviceViewModel(device: $0) } }
    }
    
    func fetchUser() -> Observable<UserViewModel> {
        modelObservable.compactMap { UserViewModel(user: $0.user) }
    }
}
