//
//  Coordinator.swift
//  moduloTechnicalTest
//
//  Created by Alban on 02/08/2021.
//

import UIKit

/// Coordinator interface
protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}
