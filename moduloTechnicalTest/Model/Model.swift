//
//  Model.swift
//  moduloTechnicalTest
//
//  Created by Alban on 02/08/2021.
//

import Foundation

struct Model: Codable {
    let devices: [Device]
    let user: User
}

struct Device: Codable {
    let id: Int
    let deviceName: String
    let intensity: Int?
    let mode: String?
    let position: Int?
    let temperature: Int?
    let productType: String
}

struct User: Codable {
    let firstName: String
    let lastName: String
    let address: Address
    let birthDate: Int
}

struct Address: Codable {
    let city: String
    let postalCode: Int
    let street: String
    let streetCode: String
    let country: String
}
