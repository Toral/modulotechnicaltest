//
//  MainCoordinator.swift
//  moduloTechnicalTest
//
//  Created by Alban on 02/08/2021.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = MainViewController()
        viewController.coordinator = self
        
        let viewModel = MainViewModel()
        viewController.viewModel = viewModel
        
        navigationController.pushViewController(viewController, animated: false)
    }
    
    func showLightVC(deviceViewModel: DeviceViewModel) {
        let viewController = LightsViewController()
        viewController.coordinator = self
        viewController.deviceViewModel = deviceViewModel
        navigationController.pushViewController(viewController, animated: true)
    }

    func showRollerShutterVC(deviceViewModel: DeviceViewModel) {
        let viewController = RollerShutterViewController()
        viewController.coordinator = self
        viewController.deviceViewModel = deviceViewModel
        navigationController.pushViewController(viewController, animated: true)
    }

    func showHeaterVC(deviceViewModel: DeviceViewModel) {
        let viewController = HeaterViewController()
        viewController.coordinator = self
        viewController.deviceViewModel = deviceViewModel
        navigationController.pushViewController(viewController, animated: true)
    }
}
