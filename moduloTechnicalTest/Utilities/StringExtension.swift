//
//  StringExtension.swift
//  moduloTechnicalTest
//
//  Created by Alban on 07/08/2021.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
