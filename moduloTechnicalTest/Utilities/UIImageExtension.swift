//
//  UIImageExtension.swift
//  moduloTechnicalTest
//
//  Created by Alban on 07/08/2021.
//

import UIKit

extension UIImage {
    func isEqualToImage(_ image: UIImage) -> Bool {
        return self.pngData() == image.pngData()
    }
}
 
