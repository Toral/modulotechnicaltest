//
//  MainService.swift
//  moduloTechnicalTest
//
//  Created by Alban on 02/08/2021.
//

import Foundation
import RxSwift

class MainService: BaseRequest {
    private static let urlData = "data.json"
    
    static let shared = MainService()
    
    public func request() -> Observable<Model> {
        return GET(MainService.urlData)
    }
}
