//
//  BaseRequest.swift
//  moduloTechnicalTest
//
//  Created by Alban on 02/08/2021.
//

import Foundation
import RxSwift

class BaseRequest {
    func GET<T: Codable>(_ urlStr: String) -> Observable<T> {
        return Observable.create { observer -> Disposable in
            guard let url = URL(string: Environment.baseURL + urlStr) else {
                observer.onError(NSError(domain: "errorUrl".localized, code: 0, userInfo: nil))
                return Disposables.create()
            }
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    let error = NSError(domain: String(format: "errorWSCall".localized, url.absoluteString), code: 0, userInfo: nil)
                    observer.onError(error)
                    return
                }
                
                do {
                    let decodable = try JSONDecoder().decode(T.self, from: data)
                    observer.onNext(decodable)
                } catch let error {
                    observer.onError(error)
                }
            }
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
}
