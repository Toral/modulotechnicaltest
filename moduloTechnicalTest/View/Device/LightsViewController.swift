//
//  LightsViewController.swift
//  moduloTechnicalTest
//
//  Created by Alban on 02/08/2021.
//

import UIKit

class LightsViewController: UIViewController {
    @IBOutlet weak var modeSwitch: UISwitch!
    @IBOutlet weak var lightSlider: VerticalSlider!
    
    weak var coordinator: MainCoordinator?
    var deviceViewModel: DeviceViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    private func setupView() {
        self.title = deviceViewModel.deviceName

        modeSwitch.isOn = deviceViewModel.device.mode == "ON"
        lightSlider.value = Float(deviceViewModel.device.intensity ?? 0)
    }
}
