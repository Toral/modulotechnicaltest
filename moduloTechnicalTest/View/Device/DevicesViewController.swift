//
//  DevicesViewController.swift
//  moduloTechnicalTest
//
//  Created by Alban on 02/08/2021.
//

import UIKit
import RxSwift

class DevicesViewController: UIViewController {
    @IBOutlet weak var devicesTableView: UITableView!
    @IBOutlet weak var heaterCheckBox: UIImageView!
    @IBOutlet weak var lightCheckBox: UIImageView!
    @IBOutlet weak var rollerCheckBox: UIImageView!
    
    weak var coordinator: MainCoordinator?
    var viewModel: MainViewModel!
    
    var disposeBag = DisposeBag()
    
    // By default all product are visible
    private var filter = ProductType.allCases

    override func viewDidLoad() {
        super.viewDidLoad()
        
        devicesTableView.register(UINib(nibName: CellIdentifier.deviceCell, bundle: nil),
                                  forCellReuseIdentifier: CellIdentifier.deviceCell)

        // We can attach one tapGesture at only one view
        let tapHeater = UITapGestureRecognizer(target: self, action: #selector(filter(_:)))
        heaterCheckBox.addGestureRecognizer(tapHeater)
        let tapLight = UITapGestureRecognizer(target: self, action: #selector(filter(_:)))
        lightCheckBox.addGestureRecognizer(tapLight)
        let tapRoller = UITapGestureRecognizer(target: self, action: #selector(filter(_:)))
        rollerCheckBox.addGestureRecognizer(tapRoller)

        viewModel.fetchDevicesViewModel()
            .bind(to:
                    devicesTableView.rx.items(cellIdentifier: CellIdentifier.deviceCell,
                                              cellType: DeviceCell.self)) { index ,viewModel, cell in
                cell.selectionStyle = .none
                cell.setupCell(deviceName: viewModel.deviceName, type: viewModel.type)
            }.disposed(by: disposeBag)
        
        self.devicesTableView.rx.modelSelected(DeviceViewModel.self).bind {[weak self] viewModel in
            switch viewModel.type {
            case .heater:
                self?.coordinator?.showHeaterVC(deviceViewModel: viewModel)
            case .rollerShutter:
                self?.coordinator?.showRollerShutterVC(deviceViewModel: viewModel)
            case .light:
                self?.coordinator?.showLightVC(deviceViewModel: viewModel)
            default:
                break
            }
        }.disposed(by: self.disposeBag)
    }
    
    
    @objc private func filter(_ sender: UITapGestureRecognizer) {
        if sender.view == heaterCheckBox {
            if isChecked(checkBox: heaterCheckBox) {
                heaterCheckBox.image = #imageLiteral(resourceName: "checkBoxUnchecked")
                filter.removeAll(where: { $0 == .heater })
            } else {
                heaterCheckBox.image = #imageLiteral(resourceName: "checkbox")
                filter.append(.heater)
            }
        } else if sender.view == lightCheckBox {
            if isChecked(checkBox: lightCheckBox) {
                lightCheckBox.image = #imageLiteral(resourceName: "checkBoxUnchecked")
                filter.removeAll(where: { $0 == .light })
            } else {
                lightCheckBox.image = #imageLiteral(resourceName: "checkbox")
                filter.append(.light)
            }
        } else if sender.view == rollerCheckBox {
            if isChecked(checkBox: rollerCheckBox) {
                rollerCheckBox.image = #imageLiteral(resourceName: "checkBoxUnchecked")
                filter.removeAll(where: { $0 == .rollerShutter })
            } else {
                rollerCheckBox.image = #imageLiteral(resourceName: "checkbox")
                filter.append(.rollerShutter)
            }
        }
    }
    
    private func isChecked(checkBox: UIImageView) -> Bool {
        return checkBox.image?.isEqualToImage(#imageLiteral(resourceName: "checkbox")) ?? false
    }
}
