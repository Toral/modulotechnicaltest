//
//  HeaterViewController.swift
//  moduloTechnicalTest
//
//  Created by Alban on 03/08/2021.
//

import UIKit
import RxSwift
import RxRelay

class HeaterViewController: UIViewController {
    @IBOutlet weak var heaterSwitch: UISwitch!
    @IBOutlet weak var heaterStepper: UIStepper!
    @IBOutlet weak var heaterValue: UILabel!
    
    weak var coordinator: MainCoordinator?
    var deviceViewModel: DeviceViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }

    private func setupView() {
        self.title = deviceViewModel.deviceName
        
        heaterSwitch.isOn = deviceViewModel.device.mode == "ON"
        heaterValue.text  = "\(deviceViewModel.device.temperature ?? 7)°"
        heaterStepper.value = Double(deviceViewModel.device.temperature ?? 7)
        deviceViewModel.temperature.bind { test in
            print(test)
        }.disposed(by: DisposeBag())
    }
    @IBAction func heaterValueChanged(_ sender: UIStepper) {
        heaterValue.text = "\(Int(sender.value))°"
        deviceViewModel.temperature.accept(Int(sender.value))
        
    }
}
