//
//  RollerShutterViewController.swift
//  moduloTechnicalTest
//
//  Created by Alban on 03/08/2021.
//

import UIKit

class RollerShutterViewController: UIViewController {
    
    @IBOutlet weak var rollerShutterSlider: VerticalSlider!
    weak var coordinator: MainCoordinator?
    var deviceViewModel: DeviceViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }
    
    private func setupView() {
        self.title = deviceViewModel.deviceName
        rollerShutterSlider.value = Float(deviceViewModel.device.position ?? 0)
    }
}
