//
//  DeviceCell.swift
//  moduloTechnicalTest
//
//  Created by Alban on 03/08/2021.
//

import UIKit

class DeviceCell: UITableViewCell {
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setupCell(deviceName: String, type: ProductType?) {
        self.deviceName.text = deviceName
        self.deviceImage.image = getImage(deviceType: type)
    }
    
    private func getImage(deviceType: ProductType?) -> UIImage? {
        switch deviceType {
        case .heater:
            return #imageLiteral(resourceName: "heater-2")
        case .rollerShutter:
            return #imageLiteral(resourceName: "roller-shutter-door")
        case .light:
            return #imageLiteral(resourceName: "light-bulb")
        default:
            return nil
        }
    }
}
