//
//  UserViewController.swift
//  moduloTechnicalTest
//
//  Created by Alban on 03/08/2021.
//

import UIKit
import RxSwift

class UserViewController: UIViewController {
    @IBOutlet weak var fisrtNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var numStreetTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var streetNameTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var birthDateTextField: UITextField!
    
    weak var coordinator: MainCoordinator?
    var viewModel: MainViewModel!
    private let disposeBag = DisposeBag()
    
    private let datePicker = UIDatePicker()
    private let dateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.fetchUser().bind {[weak self] userViewModel in
            DispatchQueue.main.async {
                self?.fisrtNameTextField.text = userViewModel.firstName
                self?.lastNameTextField.text = userViewModel.lastName
                self?.numStreetTextField.text = userViewModel.streetCode
                self?.streetNameTextField.text = userViewModel.streetName
                self?.countryTextField.text = userViewModel.country
                self?.cityTextField.text = userViewModel.city
                self?.birthDateTextField.text = userViewModel.birthDate
                self?.createDatePicker(defaultDate: userViewModel.birthDate)
            }
        }.disposed(by: disposeBag)
    }
    
    private func createDatePicker(defaultDate: String) {
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        datePicker.locale = Locale.current
        dateFormatter.dateFormat = "dd/MM/YYYY"
        datePicker.date = dateFormatter.date(from: defaultDate) ?? Date()
        birthDateTextField.inputView = datePicker
        birthDateTextField.inputAccessoryView = createToolBar()
    }
    
    private func createToolBar() -> UIToolbar{
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneAction))
        toolBar.setItems([doneButton], animated: true)
        
        return toolBar
    }
    
    @objc private func doneAction() {
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/YYYY"
        
        self.birthDateTextField.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
}

extension UserViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case fisrtNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            cityTextField.becomeFirstResponder()
        case cityTextField:
            numStreetTextField.becomeFirstResponder()
        case numStreetTextField:
            streetNameTextField.becomeFirstResponder()
        case streetNameTextField:
            countryTextField.becomeFirstResponder()
        case countryTextField:
            birthDateTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
}
