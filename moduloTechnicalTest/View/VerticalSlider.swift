//
//  VerticalSlider.swift
//  moduloTechnicalTest
//
//  Created by Alban on 03/08/2021.
//

import UIKit

class VerticalSlider: UISlider {
    /* Track IBInspectable */
    @IBInspectable var trackHeight: CGFloat = 3
    @IBInspectable var trackBackgrounColor: UIColor = .white
    @IBInspectable var trackTintColor: UIColor = .gray
    
    /* Thumb IBInspectable */
    @IBInspectable var thumbRadius: CGFloat = 20
    @IBInspectable var thumbBorderWidth: CGFloat = 2
    @IBInspectable var thumbBorderColor: UIColor = .red
    @IBInspectable var thumbColor: UIColor = .black
    
    /* Step */
    @IBInspectable var interval: Int = 1
    
    // Custom thumb view which will be converted to UIImage
    // and set as thumb. You can customize it's colors, border, etc.
    private lazy var thumbView: UIView = {
        let thumb = UIView()
        thumb.backgroundColor = thumbColor
        thumb.layer.borderWidth = thumbBorderWidth
        thumb.layer.borderColor = thumbBorderColor.cgColor
        return thumb
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let thumb = thumbImage(radius: thumbRadius)
        setThumbImage(thumb, for: .normal)
        
        self.minimumValue = 0
        self.maximumValue = 100
        self.minimumTrackTintColor = trackTintColor
        self.maximumTrackTintColor = trackBackgrounColor
        
        self.addTarget(self, action: #selector(handleValueChange(sender:)), for: .valueChanged)
        
        // Rotate vertically -90° to start from the bottom to the top
        self.transform = CGAffineTransform(rotationAngle: -(.pi/2))
        // Center the slider
        self.transform = self.transform.translatedBy(x: 0, y: -2)
    }

    private func thumbImage(radius: CGFloat) -> UIImage {
        // Set proper frame
        // y: radius / 2 will correctly offset the thumb
        
        thumbView.frame = CGRect(x: 0, y: radius / 2, width: radius, height: radius)
        thumbView.layer.cornerRadius = radius / 2
        
        // Convert thumbView to UIImage
        let renderer = UIGraphicsImageRenderer(bounds: thumbView.bounds)
        return renderer.image { rendererContext in
            thumbView.layer.render(in: rendererContext.cgContext)
        }
    }
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        // Set custom track height
        var newRect = super.trackRect(forBounds: bounds)
        newRect.size.height = trackHeight
        return newRect
    }
    
    @objc func handleValueChange(sender: UISlider) {
        let newValue = (sender.value / Float(interval)).rounded() * Float(interval)
        setValue(Float(newValue), animated: false)
    }
}
