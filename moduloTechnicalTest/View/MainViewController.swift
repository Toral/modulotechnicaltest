//
//  MainViewController.swift
//  moduloTechnicalTest
//
//  Created by Alban on 03/08/2021.
//

import UIKit
import RxSwift
import RxCocoa

class MainViewController: UITabBarController {

    weak var coordinator: MainCoordinator?
    var viewModel: MainViewModel!
    
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let devicesVC = DevicesViewController()
        devicesVC.coordinator = coordinator
        devicesVC.viewModel = viewModel
        devicesVC.tabBarItem = UITabBarItem(title: "deviceTabBar".localized, image: #imageLiteral(resourceName: "devices_unselected"), selectedImage: #imageLiteral(resourceName: "devices_selected"))
        
        let userVC = UserViewController()
        userVC.coordinator = coordinator
        userVC.viewModel = viewModel
        userVC.tabBarItem = UITabBarItem(title: "userTabBar".localized, image: #imageLiteral(resourceName: "user_unselected"), selectedImage: #imageLiteral(resourceName: "user_selected"))
        
        self.viewControllers = [devicesVC, userVC]
        
        // Set text color to a proper color (light gray unselected and black when selected item)
        UITabBarItem.appearance()
            .setTitleTextAttributes([.foregroundColor: UIColor.lightGray], for: .normal)
        UITabBarItem.appearance()
            .setTitleTextAttributes([.foregroundColor: .black | .white], for: .selected)
    }
}

extension MainViewController: UITabBarControllerDelegate {
    
}
