# ModuloTechnicalTest

Test technique pour ModuloTech réalisé par Alban PEZZOLI

## Librairie

Utilisation de la librairie RxSwift importé par Swift Package Manager

## Taches réalisées

- Récupération et parsing du fichier JSON avec URLSession et Decodable
- Création de tous les ViewControllers via des .xib
- Création d'un VerticalSlider customisable via l'interface graphique (couleur, pas et taille)
- Projet en MVVM-C et utilisation de RXSwift
- Ajout d'un fichier localizable.string et d'une extension pour rendre l'utilisation plus intuitive
- Supporte le dark mode

## Reste à faire

- Gestion des filtres et ajout d'une UISearchBar pour faire une recherche dans les devices présents
- Possibilité de supprimer un device dans la liste 
- Modification des données via RxSwift après mise à jour dans les viewController
  - Tentative avec RxRelay et BehaviorRelay
  - Redesign l'application et l'architecture pour changer les appels aux viewModels
  - Modification des viewModels pour le passage des données
- Gestion des différentes tailles d'iPhones et du mode paysage dans les viewControllers



## Débrief

- Temps total de 20h sur le projet
- Difficulté sur RxSwift
